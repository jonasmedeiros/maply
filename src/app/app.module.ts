import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { AuthGuard } from './auth/shared/services/auth-guard.service';
import { LoginService } from './auth/shared/services/login.service';
import { AppConfig, APP_CONFIG } from './app.config';
import { RouterModule, Routes } from '@angular/router';

import 'hammerjs';

const appRoutes: Routes = [];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    AppRoutingModule,
    BrowserAnimationsModule,
    AuthModule
  ],
  providers: [AuthGuard, LoginService, {
      provide: APP_CONFIG,
      useValue: AppConfig
    }],
  bootstrap: [AppComponent],
})
export class AppModule { }
