import { OpaqueToken } from '@angular/core';
import { MaplyConfig } from './shared/models/maply.config';

export let APP_CONFIG = new OpaqueToken('app.config');

export const AppConfig: MaplyConfig = {
  baseUrl: 'http://api-staging.maply.io/v1'
}