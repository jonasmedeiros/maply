import { Component, OnInit, Input, OnDestroy, forwardRef, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { LoginService } from './../../shared/services/login.service';

@Component({
  selector: 'app-reset-default',
  templateUrl: './reset-default.component.html',
  styleUrls: ['./reset-default.component.scss']
})
export class ResetDefaultComponent implements OnInit, OnDestroy  {

  @Input() form: FormGroup;

  public loginRequest; 
  public emailRegex = '^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$';

  constructor(private router: Router, 
              private route: ActivatedRoute,
              private fb: FormBuilder,
              public snackBar: MdSnackBar,
              public loginService: LoginService) { }

  ngOnInit() {
    this.form = this.fb.group({
      'email': [null, [Validators.required, Validators.pattern(this.emailRegex)]]
    });
  }

  public login() {

    if (! this.form.valid) {
      return false;
    }
    
    this.snackBar.open('Reset Successfull', null, {
      duration: 500,
    });
  }

  ngOnDestroy() {}

}
