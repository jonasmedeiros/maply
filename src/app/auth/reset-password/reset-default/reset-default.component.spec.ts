import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetDefaultComponent } from './reset-default.component';

describe('ResetDefaultComponent', () => {
  let component: ResetDefaultComponent;
  let fixture: ComponentFixture<ResetDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
