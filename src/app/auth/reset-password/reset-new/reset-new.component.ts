import { Component, OnInit, Input, OnDestroy, forwardRef, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { LoginService } from './../../shared/services/login.service';
import { matchingPasswords } from './../../shared/validators/validators';

@Component({
  selector: 'app-reset-new',
  templateUrl: './reset-new.component.html',
  styleUrls: ['./reset-new.component.scss']
})
export class ResetNewComponent implements OnInit, OnDestroy  {

  @Input() form: FormGroup;

  public loginRequest; 
  public emailRegex = '^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$';

  constructor(private router: Router, 
              private route: ActivatedRoute,
              private fb: FormBuilder,
              public snackBar: MdSnackBar,
              public loginService: LoginService) { }

  ngOnInit() {
    this.form = this.fb.group({
      'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])],
      'confirmPassword': [null, [Validators.required, Validators.minLength(6)]],
    }, {
      validator: Validators.compose([matchingPasswords('password', 'confirmPassword')])
    });
  }


  public reset() {

    if (this.form.hasError('mismatchedPasswords')) {
      this.snackBar.open('Passwords dont match', null, {
        duration: 500,
      });
      return false;
    }

    if (! this.form.valid) {
      return false;
    }
    
    this.snackBar.open('Reset Successfull', null, {
      duration: 500,
    });
  }

  ngOnDestroy() {}
}
