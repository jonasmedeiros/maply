import { Component, OnInit, Input, OnDestroy, forwardRef, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { LoginService } from './../shared/services/login.service';
import { matchingPasswords, EmailConfirmValidator } from './../shared/validators/validators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  @Input() form: FormGroup;

  public loginRequest; 
  public emailRegex = '^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$';

  constructor(private router: Router, 
              private route: ActivatedRoute,
              private fb: FormBuilder,
              public snackBar: MdSnackBar,
              public loginService: LoginService) { }

  ngOnInit() {
    this.form = this.fb.group({
      'name': [null, Validators.required],
      'lastName': [null, Validators.required],
      'email': [null, [Validators.required, Validators.pattern(this.emailRegex)]],
      'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])],
      'confirmPassword': [null, [Validators.required, Validators.minLength(6)]],
      'industry': [null, Validators.required],
    }, {
      validator: Validators.compose([matchingPasswords('password', 'confirmPassword'), EmailConfirmValidator.isMatching])
    });
  }

  public register() {

    if (this.form.hasError('mismatchedPasswords')) {
      this.snackBar.open('Passwords dont match', null, {
        duration: 500,
      });
      return false;
    }

    if (this.form.hasError('industry')) {
      this.snackBar.open('Industry is required', null, {
        duration: 500,
      });
      return false;
    }

    if (! this.form.valid) {
      return false;
    }

    this.snackBar.open('Register with success', null, {
      duration: 500,
    });
  }

  ngOnDestroy() {}

}
