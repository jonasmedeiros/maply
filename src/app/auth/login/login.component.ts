import { Component, OnInit, Input, OnDestroy, forwardRef, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { LoginService } from './../shared/services/login.service';

@Component({
  selector: 'maply-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  @Input() form: FormGroup;

  public loginRequest; 
  public emailRegex = '^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$';

  constructor(private router: Router, 
              private route: ActivatedRoute,
              private fb: FormBuilder,
              public snackBar: MdSnackBar,
              public loginService: LoginService) { }

  ngOnInit() {
    this.form = this.fb.group({
      'email': [null, [Validators.required, Validators.pattern(this.emailRegex)]],
      'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  public login() {

    if (! this.form.valid) {
      return false;
    }

    // this.loginRequest = this.route.params
    //   .switchMap((params: Params) => {
    //         return this.loginService.login(this.form.controls['email'].value, this.form.controls['password'].value);
    //     }).subscribe(
    //     (result: any) => {
    //       console.log(result);
    //     }, (error: any) => {
    //       console.log(error);
    //     });
    
    this.snackBar.open('Login have been succesfull', null, {
      duration: 500,
    });
  }

  ngOnDestroy() {}

}
