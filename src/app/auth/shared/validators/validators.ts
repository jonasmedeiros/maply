import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

/*
  Custom validators to use everywhere.
*/

// SINGLE FIELD VALIDATORS
export function emailValidator(control: FormControl): {[key: string]: any} {
  var emailRegexp = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
  if (control.value && !emailRegexp.test(control.value)) {
    return { invalidEmail: true };
  }
}

// FORM GROUP VALIDATORS
export function matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
  return (group: FormGroup): {[key: string]: any} => {
    let password = group.controls[passwordKey];
    let confirmPassword = group.controls[confirmPasswordKey];
    
    if (password.value !== confirmPassword.value) {
      return {
        mismatchedPasswords: true
      };
    }
  }
}

// FORM GROUP VALIDATORS
export class EmailConfirmValidator {
 
  static isMatching(group: FormGroup){
    var email = group.controls['email'].value;
    var confirmation = group.controls['emailConfirmation'] ? group.controls['emailConfirmation'].value : '';
    
    if((email && confirmation) && (email !== confirmation)){
      return { "email_mismatch": true };      
    } else{
      return null;
    }
    
  }
 
}