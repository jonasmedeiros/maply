import { LoginModel } from './login.model';

export interface AuthModel {
  auth: LoginModel;
}
