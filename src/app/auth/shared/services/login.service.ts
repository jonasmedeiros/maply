import { Injectable, Inject } from '@angular/core';
import { Http, Response, RequestOptions, Headers, Request, RequestMethod, URLSearchParams } from '@angular/http';
import { APP_CONFIG, AppConfig } from './../../../app.config'
import { LoginModel } from './../models/login.model'
import { AuthModel } from './../models/auth.model'
import { MaplyConfig } from './../../../shared/models/maply.config'
import { Observable, Subject } from 'rxjs';
import { AuthHttp, tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class LoginService {

  private baseUrl: string;
  private clientId: string;
  private clientSecret: string;
  private logged = new Subject();

  constructor(@Inject(APP_CONFIG) private config: MaplyConfig, private http: Http, public authHttp: AuthHttp) {
    this.baseUrl = config.baseUrl;
  }

  /**
   * Method login.
   *
   * Validate login
   *
   * @param email string validated from form
   * @param password string
   *
   * @example login('foo@temp.com', 'foo123');
   */
  public login(email: string, password: string): Observable<LoginModel> {

    let headers = new Headers();
    headers.append('content-type', 'application/json');

    let options = new RequestOptions({ headers: headers});
    
    let body: AuthModel = {
      auth : {
        email: email,
        password: password
      }
    }

    let endpoint = `${this.baseUrl}/user_token`;
    
    return this.authHttp.post(endpoint, body, options)
                    .map((response: Response) => this.mapResponse(response.json()));
  }

  private mapResponse(response: LoginModel): LoginModel {

    this.setLogin(response);
    return response;
  }

  /**
   * Method logout.
   *
   * Remove user from localstorage and customer
   *
   * @return should return json today is going to another page
   *
   * @example logout();
   */
  public logout() {
    
    this.logged.next(false);

    localStorage.removeItem('user');
  }

  public loggedIn() {

    console.log(tokenNotExpired());


    //return tokenNotExpired();
  }

  /**
   * Method authorized.
   *
   * Client side Check if user is logged
   *
   * @return return boolean true or false
   *
   */
  public isLoggedIn():Observable<boolean> {

    if (localStorage.getItem('user') === null || localStorage.getItem('user') === undefined) {
      return Observable.of(false);
    }

    let currentUser = this.getLoggedUser();
        
    let headers = new Headers({
      'Authorization': `Bearer ${currentUser.access_token}`,
    });

    let endpoint = `${this.baseUrl}/user/me`;

    return this.http.get(endpoint, { headers: headers }).map(response => {

      return true;
    });
  }

  // this method does not check the server only local storage
  public isLogged():boolean {

    if (localStorage.getItem('user') === null || localStorage.getItem('user') === undefined) {
      return false
    }

    let currentUser = this.getLoggedUser();
    

    if (currentUser.access_token) {
      return true;
    }

    return false;
  }

  public observeLogged():Subject<boolean> {
      return this.logged;
  }

  /**
   * Method getLoggedUser.
   *
   * return user object
   *
   * @return return user object from localstorage
   *
   */
  public getLoggedUser() {

    let user = localStorage.getItem('user');

    if (user !== undefined) {
      return JSON.parse(user);
    }

    return {};
  }

  private setLogin(login: LoginModel) {

    this.logged.next(true);

    localStorage.setItem('user', JSON.stringify(login));
  }

}
