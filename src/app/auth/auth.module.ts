import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { AuthJWModule } from './../auth.jw.module';
import { HttpModule } from '@angular/http';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ResetNewComponent } from './reset-password/reset-new/reset-new.component';
import { ResetDefaultComponent } from './reset-password/reset-default/reset-default.component';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';

const appRoutes: Routes = [];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    AuthJWModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  declarations: [LoginComponent, LayoutComponent, ResetPasswordComponent, ResetNewComponent, ResetDefaultComponent, RegisterComponent],
  exports: [LoginComponent, LayoutComponent, ResetPasswordComponent, ResetNewComponent, ResetDefaultComponent, RegisterComponent]
})
export class AuthModule { }
