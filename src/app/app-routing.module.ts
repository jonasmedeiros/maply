import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { ResetNewComponent } from './auth/reset-password/reset-new/reset-new.component';
import { ResetDefaultComponent } from './auth/reset-password/reset-default/reset-default.component';
import { RegisterComponent } from './auth/register/register.component';
import { AuthGuard } from './auth/shared/services/auth-guard.service';

export class RouterPaths {
  static readonly LOGIN = 'login';
  static readonly REGISTER = 'register';
  static readonly RESET = 'reset-password';
  static readonly RESET_NEW = 'reset-password-new';
  static readonly LOGOUT = 'logout';
}

const appRoutes: Routes = [
  // { path: 'admin', component: AdminComponent, canActivate: [AuthGuard] },
  { path: RouterPaths.LOGIN, component: LoginComponent },
  { path: RouterPaths.REGISTER, component: RegisterComponent },
  { path: RouterPaths.LOGOUT, component: LoginComponent },
  { path: RouterPaths.RESET, component: ResetDefaultComponent },
  { path: RouterPaths.RESET_NEW, component: ResetNewComponent },
  { path: '**', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
